import sys

import pytest

sys.path.append("/usr/src/app/src")
from constansts import BOUNDARIES, END, LETTER, PAUSE, PUNCTUATION, START

from fixture import converter


@pytest.mark.parametrize(
    "input, ground_truth",
    [
        ("лиза пошла в кафе", f"{START}из{END} {START}ошл{END} {START} {START}аф{END}"),
        ("voldermort dead", f"{START}oldermor{END} {START}ea{END}"),
        ("а", f"{START}"),
        ("аи", f"{START}{END}"),
        (
            "La  La  лягди(!*?4яяяяя   ",
            f"{START}{END}  {START}{END}  {START}ягди(!*?4яяяя{END}   ",
        ),
        ("  h", f"  {START}"),
        ("f  ", f"{START}  "),
        ("  fh.", f"  {START}{END}."),
        ("fh  .", f"{START}{END}  ."),
    ],
)
def test_convert_start_end_tokens(converter, input, ground_truth):
    """
    Checking convert_start_end_tokens function of Converter class. We are checking diffrent laguages and amount of spaces.
    """
    answer = converter.convert_start_end_tokens(input)
    assert answer == ground_truth


@pytest.mark.parametrize(
    "input, ground_truth",
    [
        (
            "лиза пошла в кафе",
            f"{BOUNDARIES}из{BOUNDARIES} {BOUNDARIES}ошл{BOUNDARIES} {BOUNDARIES} {BOUNDARIES}аф{BOUNDARIES}",
        ),
        ("  a  ", f"  {BOUNDARIES}  "),
        ("a", f"{BOUNDARIES}"),
        ("ab", f"{BOUNDARIES}{BOUNDARIES}"),
        ("   lozala.", f"   {BOUNDARIES}ozal{BOUNDARIES}."),
    ],
)
def test_convert_word_boundaries(converter, input, ground_truth):
    """
    Checking convert_word_boundaries function of Converter class.
    """
    answer = converter.convert_word_boundaries(input)
    assert answer == ground_truth


@pytest.mark.parametrize(
    "input, ground_truth",
    [
        ("f231    f", f"{LETTER}{LETTER}{LETTER}{LETTER}    {LETTER}"),
        ("f.", f"{LETTER}."),
        ("  f  .  ", f"  {LETTER}  .  "),
        (" .", " ."),
    ],
)
def test_convert_letters(converter, input, ground_truth):
    """
    Checking convert_letters function of Converter class.
    """
    answer = converter.convert_letters(input)
    assert answer == ground_truth


@pytest.mark.parametrize(
    "input, ground_truth",
    [
        ("f231    f", f"f231{PAUSE}{PAUSE}{PAUSE}{PAUSE}f"),
        ("f.", f"f."),
        ("  f  .  ", f"{PAUSE}{PAUSE}f{PAUSE}{PAUSE}.{PAUSE}{PAUSE}"),
        (" .", f"{PAUSE}."),
    ],
)
def test_convert_pauses(converter, input, ground_truth):
    """
    Checking convert_pauses of Converter class.
    """
    answer = converter.convert_pauses(input)
    assert answer == ground_truth


@pytest.mark.parametrize(
    "input, ground_truth",
    [
        ("a", "a"),
        ("ab.", f"ab{PUNCTUATION}"),
        ("la .   la. la.", f"la {PUNCTUATION}   la{PUNCTUATION} la{PUNCTUATION}"),
        (".", f"{PUNCTUATION}"),
    ],
)
def test_convert_punctuation(converter, input, ground_truth):
    """
    Checking convert_punctuation function of Converter class.
    """
    answer = converter.convert_punctuation(input)
    assert answer == ground_truth


@pytest.mark.parametrize(
    "input, modes, ground_truth",
    [
        (
            "la .",
            [f"{START}" + f"{END}", f"{PAUSE}", f"{PUNCTUATION}"],
            f"{START}{END}{PAUSE}{PUNCTUATION}",
        ),
        (
            "laf .",
            [f"{BOUNDARIES}", f"{LETTER}", f"{PAUSE}", f"{PUNCTUATION}"],
            f"{BOUNDARIES}{LETTER}{BOUNDARIES}{PAUSE}{PUNCTUATION}",
        ),
        ("la .", [f"{LETTER}", f"{PAUSE}"], f"{LETTER}{LETTER}{PAUSE}."),
        (
            "lal   . ",
            [f"{BOUNDARIES}", f"{PAUSE}"],
            f"{BOUNDARIES}a{BOUNDARIES}{PAUSE}{PAUSE}{PAUSE}.{PAUSE}",
        ),
    ],
)
def test_convert(converter, input, modes, ground_truth):
    """
    Checking main function of Converter class called convert.
    """
    answer = converter.convert(input, modes)
    assert answer == ground_truth
