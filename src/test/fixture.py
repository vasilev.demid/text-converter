import sys

import pytest

sys.path.append("/usr/src/app/src")

from converter import Converter


@pytest.fixture
def converter():
    converter_class = Converter()
    return converter_class
