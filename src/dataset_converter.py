import json
from os import listdir

from converter import Converter


class DatasetConverter:
    def __init__(
        self, reading_path: str, tokens_to_use: list, writing_path: str
    ) -> None:
        """
        reading_path: path to folder, where all json files are contained
        tokens_to_use: to which tokens do we transfrom texts in said jsons
        writing_path: in which folder do we write changed jsons
        """
        self.reading_path = reading_path
        self.tokens_to_use = tokens_to_use
        self.writing_path = writing_path
        self.converter = Converter()

    def dataset_transformation(self) -> list:
        """
        We are reading json files from initilised path folder, transform texts in them to tokens initialised.
        Then we substitute texts in audio files into tokens and create new json files.
        """
        all_reading_files = listdir(self.reading_path)
        for file_name in all_reading_files:
            all_transformed_dataset = []
            with open(self.reading_path + file_name, "r") as f:
                all_lines = f.readlines()
                all_texts = []
                all_data = []
                for one_line in all_lines:
                    data = json.loads(one_line)
                    all_texts.append(data["text"])
                    all_data.append(data)

            for text_data in all_texts:
                transformed_text = self.converter.convert(text_data, self.tokens_to_use)
                all_transformed_dataset.append(transformed_text)

            all_texts.clear()
            all_transformed_data = []
            num_text = 0
            for dict in all_data:
                dict["text"] = all_transformed_dataset[num_text]
                num_text += 1
                all_transformed_data.append(dict)
            all_data.clear()

            writing_to_file = ""
            for dict_to_write in all_transformed_data:
                one_file = json.dumps(dict_to_write)
                writing_to_file += one_file
                writing_to_file += "\n"
            with open(self.writing_path + file_name, "w") as f:
                f.write(writing_to_file)
