import re

from constansts import BOUNDARIES, END, LETTER, PAUSE, PUNCTUATION, START, START_END_STRATEGY


class Converter:
    def __init__(
        self,
        letter_token=LETTER,
        pause_token=PAUSE,
        word_start_token=START,
        word_end_token=END,
        punctuation_token=PUNCTUATION,
        word_boundaries=BOUNDARIES,
    ):
        """
        The tokens can be renamed, but when using class function convert(), new names of tokens must be put in the list
        """
        self.letter_token = letter_token
        self.pause_token = pause_token
        self.word_start_token = word_start_token
        self.word_end_token = word_end_token
        self.punctuation_token = punctuation_token
        self.word_boundaries = word_boundaries
        self.tokens_converter = {
            self.word_start_token + self.word_end_token: self.convert_start_end_tokens,
            self.word_boundaries: self.convert_word_boundaries,
            self.letter_token: self.convert_letters,
            self.pause_token: self.convert_pauses,
            self.punctuation_token: self.convert_punctuation,
        }

    def convert(self, text: str, all_tokens_to_convert: list) -> str:
        """
        Main function to convert text from one format to another, that we will use in our model.
        Warning! Text must contain only letters, commas, spaces and dots! Please make sure that it is correct
        params: text - the text that we want to convert
        params: all_tokens_to_convert: list of str tokens that should contain tokens from __init__ from our class.
        return: converted text
        """
        # Check all other modes and convert into tokens everything we wish
        for mode in self.tokens_converter:
            if mode in all_tokens_to_convert:
                func_to_convert = self.tokens_converter[mode]
                text = func_to_convert(text)

        return text

    def convert_start_end_tokens(self, text: str) -> str:
        """
        Function, that find all starts and ends of words and transforms them in desired output.
        We assume that punctuation is depicted in dot character.
        """
        all_punct_occurances = []
        if "." in text:
            all_punct_occurances = [
                index for index, char in enumerate(text) if char == "."
            ]
        transformed_text = text.replace(".", " ")
        # Exception
        if len(text) == 1:
            return self.word_start_token

        # it's the end of the word
        transformed_text = re.sub("[^ .] ", f"{self.word_end_token} ", transformed_text)
        # changing into token the first character of the word
        transformed_text = re.sub(
            " [^ .]", f" {self.word_start_token}", transformed_text
        )
        # check first and last characters (because there may be no whitespaces there)
        if transformed_text[0] != " ":
            transformed_text = self.word_start_token + transformed_text[1:]
        if transformed_text[-1] != " " and transformed_text[-2] != " ":
            transformed_text = transformed_text[:-1] + self.word_end_token
        elif transformed_text[-1] != " " and transformed_text[-2] == " ":
            transformed_text = transformed_text[:-1] + self.word_start_token

        if len(all_punct_occurances) > 0:
            all_text = list(transformed_text)
            for one_id in all_punct_occurances:
                all_text[one_id] = "."
            transformed_text = "".join(all_text)
        return transformed_text

    def convert_word_boundaries(self, text: str) -> str:
        """
        Function, that does the same as convert_start_end_tokens(), but doesn't distinguish start and end of
        the word - we simply replace them in one token. We assume that punctuation is depicted in dot character.
        """
        all_punct_occurances = []
        if "." in text:
            all_punct_occurances = [
                index for index, char in enumerate(text) if char == "."
            ]
        transformed_text = text.replace(".", " ")

        # it's the end of the word
        transformed_text = re.sub(
            "[^ .] ", f"{self.word_boundaries} ", transformed_text
        )
        # changing into token the first character of the word
        transformed_text = re.sub(
            " [^ .]", f" {self.word_boundaries}", transformed_text
        )
        # check first and last characters (because there may be no whitespaces there)
        if transformed_text[0] != " ":
            transformed_text = self.word_boundaries + transformed_text[1:]
        if transformed_text[-1] != " ":
            transformed_text = transformed_text[:-1] + self.word_boundaries

        # don't forget to get punctuation back if we had it. Warning!!! We assume that punctuation is only dot charcter
        if len(all_punct_occurances) > 0:
            all_text = list(transformed_text)
            for one_id in all_punct_occurances:
                all_text[one_id] = "."
            transformed_text = "".join(all_text)
        return transformed_text

    def convert_letters(self, text: str) -> str:
        """
        Function to transform all letters in the text into some token. We assume that punctuation is depicted in dot character.
        """
        all_punct_occurances = []
        if "." in text:
            all_punct_occurances = [
                index for index, char in enumerate(text) if char == "."
            ]
        transformed_text = text.replace(".", " ")
        exceptions_regex = "[^ "

        def replace_group(part: str):
            return self.letter_token * len(part.group())

        # we make exceptions with all tokens that we use in regex + exception with space (we assume that
        # we only have words, tokens and pauses). It helps us only replace the letters, which is exactly what we need
        for token in self.tokens_converter:
            if token != self.letter_token:
                # to take in account that strat_end tokens are merged
                len_token = len(token)
                for part_of_token in token:
                    exceptions_regex += part_of_token
        exceptions_regex += "]*?"
        transformed_text = re.sub(exceptions_regex, replace_group, transformed_text)

        if len(all_punct_occurances) > 0:
            all_text = list(transformed_text)
            for one_id in all_punct_occurances:
                all_text[one_id] = "."
            transformed_text = "".join(all_text)
        return transformed_text

    def convert_pauses(self, text: str) -> str:
        """
        Convert all spaces between words into token.
        """
        text = text.replace(" ", self.pause_token)
        return text

    def convert_punctuation(self, text: str) -> str:
        """
        Convert all punctuation in the text into token.
        """
        text = text.replace(".", self.punctuation_token)
        return text
