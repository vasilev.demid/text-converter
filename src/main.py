import json
import os

from constansts import BOUNDARIES, END, LETTER, PAUSE, PUNCTUATION, START
from dataset_converter import DatasetConverter

if __name__ == "__main__":
    script_dir = os.path.dirname(__file__)
    # RUSPEECH_dev_manifest.json
    file_path = os.path.join(script_dir, "./data/input/")
    writing_path = os.path.join(script_dir, "./data/output/")
    token_we_use = [START + END, PAUSE, PUNCTUATION, LETTER]
    dataset_converter = DatasetConverter(file_path, token_we_use, writing_path)
    transformed_texts = dataset_converter.dataset_transformation()
