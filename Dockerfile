FROM ubuntu:20.04

RUN apt-get update -y && apt-get upgrade -y && apt-get install -y --no-install-recommends python3 python3-dev python3-pip

COPY requirements.txt /opt/requirements.txt
RUN python3 -m pip install -r /opt/requirements.txt

WORKDIR /usr/src/app
ENTRYPOINT [ "/bin/bash" ]
